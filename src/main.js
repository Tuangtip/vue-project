// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


// import axios from 'axios'
// import VueAxios from 'vue-axios'
 
// Vue.use(VueAxios, axios)

const geolib = require('geolib');

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBYvKZCFEIRUB09PS1RKwhf-VDcRVSAjuo',
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
});

import VueResource from 'vue-resource';

Vue.use(VueResource);

// axios.defaults.baseURL = 'https://maps.googleapis.com/maps/api';

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  
})
